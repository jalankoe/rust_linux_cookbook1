// use Rc RefCell
use std::rc::Rc;
use std::cell::RefCell;

///
/// # data structure 1
///
//struct Student<'a> {
struct Student {
    name: String,
    
    //courses: Vec<&'a Course<'a>>
    
    /// 
    /// using refCell courses
    ///
    courses: Vec<Rc<RefCell<Course>>>
}

struct Service {
    name: String
}

///
/// #ds 1 methods
///
//impl<'a> Student<'a>{
impl Student {
//    fn new(name: &str) -> Student<'a> {
    fn new(name: &str) -> Student {
        Student{ name: name.into(), 
                 courses: Vec::new() }
    }
}

impl Service {
    fn managers(&self, platform: Platform) -> 
        Vec<String> {
            platform.handlers.iter()
                .filter(|&e| e.service.name == self.name )
                .map(|e| e.manager.name.clone())
                .collect()
        }
}

///
/// #data structure 2
///
//struct Course<'a> {
struct Course {
    name: String,
    
    //students: Vec<&'a Student<'a>>
    
    /// 
    /// using reCell students
    ///
    students: Vec<Rc<RefCell<Student>>>
}

struct DeviceManager {
    name: String
}

struct DeviceHandler<'a> {
    service: &'a Service,
    manager: &'a DeviceManager
}

///
/// #ds 2 methods
///
//impl<'a> Course<'a> //
impl Course {
//    fn new(name: &str) -> Course<'a> {
    fn new(name: &str) -> Course {
        Course { name: name.into(), 
                 students: Vec::new()}
    }

    //fn add_student(&'a mut self, student: &'a Student<'a>) {
        // ### borrowed here
      //  student.courses.push(self);

        //
        // ### ! this is wrong use of rust
        // ### solved with a ReCell
        //
        // self.students.push(students);
   // }

    fn add_student(
        course: Rc<RefCell<Course>>,
        student: Rc<RefCell<Student>>) {
        
        student.borrow_mut()
            .courses.push(course.clone());
        course.borrow_mut().students.push(student);
    }
}

impl<'a> DeviceHandler<'a> {
    fn new(service: &'a Service,
           manager: &'a DeviceManager) -> 
        DeviceHandler<'a> {
            DeviceHandler { service, manager }        
        }
}

struct Platform<'a> {
    handlers: Vec<DeviceHandler<'a>>
}

/// platform
impl<'a> Platform<'a> {
    fn new() -> Platform<'a> {
        Platform{ handlers: Vec::new() }
    }

    fn enroll(&mut self,
              service: &'a Service,
              manager: &'a DeviceManager) {
        self.handlers.push(
            DeviceHandler::new(service,manager))
    }
}


fn main() {

    println!("Abstraction OOP with Rust!");

    //let john = Student::new("John");

    let john = Rc::new(
                RefCell::new(
                Student::new("John")
                ));


    let jane = Rc::new(
                RefCell::new(
                Student::new("Jane")
                ));

    let course = Course::new("Software");
    let course_wr = Rc::new(
                    RefCell::new(course)
                    );

    //course.add_student(john);
    
    Course::add_student(course_wr.clone(), john);
    Course::add_student(course_wr, jane);

    let john_1 = Service{ name: "John".into() };
    let manager_1 = DeviceManager{
        name: "Daemon".into()};

    let mut p = Platform::new();
    p.enroll(&john_1, &manager_1);

    for c in john_1.managers(p) {
        println!("john is taking {}", c);
    }
}
