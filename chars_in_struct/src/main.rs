
/// 
/// seperate modules
///
mod package {

    /// wrap with struct
    ///
    #[derive(Debug)]
    pub struct CharsWrapp {
        ///
        /// make a chars
        ///
        v : &'static str,
    }

    ///
    /// # implement new method
    ///
    impl CharsWrapp {
        pub fn new ( s: &'static str ) -> CharsWrapp {
            let x = CharsWrapp { v: s };
            x
        }
    }
}

///
/// # simple use of using chars in a struct
///
fn main() {
    println!("Trying characters!");

    ///
    /// allocate heap
    ///
    let x = package::CharsWrapp::new("hello characters");

    ///
    /// print
    ///
    println!("{:?}", x);
}
