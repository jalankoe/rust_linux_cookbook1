extern crate os_release;

use os_release::OsRelease;
use std::io;

pub fn main() -> io::Result<()> {
    let release = OsRelease::new_from("/etc/os-release")?;
    println!("{}", release.pretty_name);
    Ok(())
}
