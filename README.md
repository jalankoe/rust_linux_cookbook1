## The Cookbook Linux System Programming with Rust

---

## Cookbook Method

A cookbook in this programming contect is the collection of small programs to learn linux system programming. This starts with a hello program and learn system administrations with rust

This learning method will also helps as a future reference

## What is Linux Sytem Programming

Systems programming, or system programming, is the activity of programming[1] computer system software. The primary distinguishing characteristic of systems programming when aims to produce software and software platforms which provide services to other software. {https://en.wikipedia.org/wiki/Systems_programming}

## Extent of Linux Administration 

1. Making application with commandline interface
2. Opening a process, shutting down a process, and shutdown system
3. looking up system information
4. TCP Connection
5. Websockets
6. D-Bus

## Extent the of Rust Programming 

1. Writing a std out print line and print format
2. Writing a file
3. Opening a file
4. Opening an XML
5. Writing an XML

