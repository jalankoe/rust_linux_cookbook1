extern crate clap;
use clap::{App, Arg};

// #simple collecting command line argument
//
//use std::env;
//fn main() {
//    let args: Vec<String> = env::args().collect();
//    println!("{:?}", args);
//}
//
// #end

fn main() {
    // naming an application
    App::new("simple_cli")
        // adding a version
        .version("0.0.1")
        // comment in runtime
        .about("only do simple command line parsing!")
        // adding author
        .author("Dimas Fajar.")
        // collecting information
        .get_matches();
}
